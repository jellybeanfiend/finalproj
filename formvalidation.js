function removeThis(element)
{
	// remove the parent of the element
	$(element).parent().slideUp(function() { 
		$(this).remove(); 
		validateUrl();
	});
}

function validateUrl(){
		// Assume there are no empty or invalid inputs
		var valid = true;

		// Make sure each url input contains a valid url
		$(".urlInput").each(function() {

			$(this).filter(function() {
				if(!$(this).val().match(/https?:\/\/.+\..+|www\..+\..+/)){
					valid = false;
				}
			});
			console.log("after url check ",valid);
		});

		// Make sure there are no empty inputs
		$(":input").each(function() {
		   if($(this).val() === "")
		   		valid = false;
		});

		console.log("after input check ",valid);

		// If anything is empty or invalid, don't let the user submit
		if(valid == false)
			$('input[type=submit]').attr('disabled', 'disabled');
		else
		$("input[type=submit]").removeAttr("disabled");

}

$(document).ready(function() {

	// Disable the submit button immediately - fields are probably blank
	$('input[type=submit]', this).attr('disabled', 'disabled');

	// When "add" text is clicked, make another input box available for website or keyword fields.
	$('.addField').click(function(e) {
		// Prevent link from opening a page
		e.preventDefault();
		// Get the location of the previous list
		var ulElement = $(this).parent().prev();
		// Copy the first li of the previous list
		var cloned = ulElement.children('li:first-child').clone();
		// Make sure the new input is blank
		$(cloned).children('input').val('');
		// Add the link to remove this new list element
		$(cloned).append('<a href="#" class="removeMe">Remove</a>');
		// Add this new list element to the List
		$(cloned).hide();
		// make this new input respond to blur
		$(cloned).children('li:first-child > input').blur(function() {
				validateUrl(this);
			});
		ulElement.append(cloned);

		$(cloned).slideDown();
		
		$('input[type=submit]').attr('disabled', 'disabled');
		// Add functionality to remove link, using the removeThis function
		$(cloned).find('.removeMe').click(function() { removeThis(this); });
		$(cloned).find('.urlInput').blur(function() { validateUrl(this); });
	});
	
	// Every time the user clicks out of an input box, check to see if submit button can be enabled
	$("input").blur(function() {
		console.log('this wawawarks');
			validateUrl(this);
		});	

	$("input").live("keyup", function() {
		console.log('this wawawarks');
			validateUrl(this);
		});	

	// Input slider for user to select how many pages they want to search
	$(function() {
		$("#slider").slider({
	      min: 5,
	      max: 50,
	      step: 5,
	      animate: true,
	      slide: function( event, ui){
	        $('#depth').val(ui.value);
	      }
	    });
	    $( "#depth" ).val( $( "#slider" ).slider( "value" ) );
	});

});

